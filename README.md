# openSMART
SMART 600 series custom driver
# where is the code?
It's WIP, but it's here. Feel free to test it, suggest improvements, or contribute.

You should still read the Wiki to get an understanding of the protocol, 
or look at doc/smart-protocol.txt for quick reference
# how can I help?
Test, file bug reports, suggest features, contribute code, 
verify assumptions about the protocol, document protocol for different series 
(600 and 800 are documented at the moment).
