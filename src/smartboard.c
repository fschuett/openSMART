#include "defines.h"

#include <stdio.h>
#include <unistd.h>
#include <stdbool.h>

#include "uhid.h"

extern bool expose_leds;

static int prefix_length(unsigned char type){
	switch(type){
	case SB600:
		return 0;
	case SB800:
		return 2;
	}
	return 0;
}

static unsigned char compute_checksum(unsigned char *data, int length) {
	int i;
	unsigned char XOR;
	unsigned char c;

	for (XOR = 0, i = 1; i < length; i++) {
		c = (unsigned char)data[i];
		XOR ^= c;
	}
	return XOR;
}

static unsigned char encode_length(int length) {
	unsigned char result;
	result = (unsigned char)length ^ '\x0f';
	result <<= 4;
	result += length;
	return result;
}

static int send_command(int smartboard_fd, unsigned char command, unsigned char *data, int data_length) {
	if(data_length > 18-4) {
		fprintf(stderr, "error: send_command called with data_length of %d (max is 18-4=14)\n", data_length);

		return -1;
	}

	int length;
	unsigned char report[64] = {0}; //NO SPACE FOR NULL BYTE, don't handle as string
	report[0] = REPORT_ID;
	report[1] = encode_length(data_length);
	report[2] = command;
	int i;
	for (i = 0; i < data_length; i++) {
		report[i+3] = data[i];
	}
	length = data_length + 4; //4 bytes other than data
	report[length-1] = compute_checksum(report, length-1);

	write(smartboard_fd, report, 64);
	return length;
}
static int send_prefixed_command(int smartboard_fd, char boardtype, unsigned char command, unsigned char *data, int data_length) {
	int prefix = prefix_length(boardtype);
	if(command == 0x07){
		prefix++;
	}
	unsigned char pdata[data_length + prefix];
	int i;
	for(i = 0; i < data_length; i++) {
		pdata[i+prefix] = data[i];
	}
	switch(boardtype){
	case SB600:
		switch(command){
		case 0x07:
			pdata[0] = '\xff';
			break;
		}
		break;
	case SB800:
		pdata[0] = '\x16';
		pdata[1] = '\x00';
		switch(command){
		case 0x07:
			pdata[2] = '\x0f';
			break;
		}
		command = command | '\xd0';
		break;
	}
	return send_command(smartboard_fd, command, pdata, data_length + prefix);
}

void switch_leds(int smartboard_fd, char boardtype, int current_tool) {
	unsigned char data[1] = "\x00";
	if(current_tool) {
		data[0] = '\x20'; // 0b00100000
		data[0] = data[0] >> current_tool;
	}
	send_prefixed_command(smartboard_fd, boardtype, '\x07', data, 1);
}

void tools_changed(int uhid_fd, int smartboard_fd, char boardtype, int color, unsigned char tools) {

	static int previous_tool;
	int current_tool = 0;

	if (!tools) {
		current_tool = 0;
	}
	switch(boardtype){
	case SB600:
		if (tools & 0b00010000) { //BLACK
			current_tool = 1;
		}
		if (tools & 0b00001000) { //RED
			current_tool = 2;
		}
		if (tools & 0b00000010) { //GREEN
			current_tool = 4;
		}
		if (tools & 0b00000001) { //BLUE
			current_tool = 5;
		}
		if (tools & 0b00000100) { //ERASER
			current_tool = 3;
		}
		break;
	case SB800:
		if (tools & 0b00000100) { //PEN1
			current_tool = 1;
		}
		if (tools & 0b00000010) { //ERASER
			current_tool = 3;
		}
		if (tools & 0b00000001) { //PEN2
			current_tool = 2;
		}
		break;
	}

	if(current_tool == previous_tool) { //actually, no change has happened
		return;
	}

	switch(boardtype){
	case SB600:
		switch (current_tool) {
		case 1:
			send_key(uhid_fd, 0x04);
			break;
		case 2:
			send_key(uhid_fd, 0x05);
			break;
		case 3:
			send_key(uhid_fd, 0x06);
			break;
		case 4:
			send_key(uhid_fd, 0x07);
			break;
		case 5:
			send_key(uhid_fd, 0x08);
			break;
		}
		break;
		case SB800:
			switch (current_tool) {
				case 1:
				case 2:
					switch (color) {
					case 2:
						send_key(uhid_fd, 0x04);
						break;
					case 3:
						send_key(uhid_fd, 0x05);
						break;
					case 4:
						send_key(uhid_fd, 0x07);
						break;
					case 5:
						send_key(uhid_fd, 0x08);
						break;
					}
					break;
				case 3:
					send_key(uhid_fd, 0x06);
					break;
			}
			break;
 	}

	if (!expose_leds) {
		switch(boardtype){
		case SB600:
			switch_leds(smartboard_fd, boardtype, current_tool);
			break;
		case SB800:
			switch_leds(smartboard_fd, boardtype, color);
			break;
		}
	}
	
	previous_tool = current_tool;
}

void buttons_changed(int uhid_fd, int smartboard_fd, char boardtype, int *color, unsigned char buttons) {

	int current_button = 0;

	if (!buttons) {
		current_button = 0;
	}
	switch(boardtype){
	case SB600:
		if (buttons & 0b00000001) { //KEYBOARD
			current_button = 1;
		}
		if (buttons & 0b00000010) { //RIGHT_CLICK
			current_button = 2;
		}
		if (buttons & 0b00000100) { //HELP
			current_button = 3;
		}
		break;
	case SB800:
		if (buttons & 0b00100000) { //KEYBOARD
			current_button = 1;
		}
		if (buttons & 0b00000001) { //RIGHT_CLICK
			current_button = 2;
		}
		if (buttons & 0b01000000) { //HELP
			current_button = 3;
		}
		if (buttons & 0b10000000) { //CALIBRATE
			current_button = 4;
		}
		if (buttons & 0b00000010) { //BLUE
			current_button = 8;
			(*color) = 5;
		}
		if (buttons & 0b00000100) { //GREEN
			current_button = 7;
			(*color) = 4;
		}
		if (buttons & 0b00001000) { //RED
			current_button = 6;
			(*color) = 3;
		}
		if (buttons & 0b00010000) { //BLACK
			current_button = 5;
			(*color) = 2;
		}
		break;
	}

	switch (current_button) {
		case 1:
			/*TODO launch (on-screen keyboard) program specified in config*/
			printf("STUB: TODO launch (on-screen keyboard) program specified in config\n");
			break;
		case 2:
			/*TODO send right click event*/
			printf("STUB: TODO send right click event\n");
			break;
		case 3:
			/*TODO launch (help) program specified in config*/;
			printf("STUB: TODO launch (help) program specified in config\n");
			break;
		case 4:
			/*TODO launch (calibrate) program specified in config*/;
			printf("STUB: TODO launch (calibrate) program specified in config\n");
			break;
		case 5:
		case 6:
		case 7:
		case 8:
			if( !expose_leds ){
				switch_leds(smartboard_fd, boardtype, *color);
			}
			break;
	}
}

int process_command(int uhid_fd, int smartboard_fd, unsigned char command, unsigned char *data) {
	static char boardtype;
	static int color;
	switch(command & 0x0f) {
		case 0x0: /* CHECK DRIVER PRESENCE */
			boardtype = data[0];
			unsigned char response[1] = "\x01";
			send_command(smartboard_fd, '\x00', response, 1);
			break;
		case 0x05: /* PENTRAY STATUS */
			tools_changed(uhid_fd, smartboard_fd, boardtype, color, data[prefix_length(boardtype)]);
			break;
		case 0x06: /* Buttons */
			buttons_changed(uhid_fd, smartboard_fd, boardtype, &color, data[prefix_length(boardtype)]);
			break;
	}
	return 0;
}
